package main

import (
	"encoding/json"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"net/http"
	"os"
	"strconv"
	"sync"
)

var topURL = "https://hacker-news.firebaseio.com/v0/topstories.json"

// News provides hackernews story fields
type News struct {
	Title string
	URL   string
}

func main() {
	var top [30]int
	var news News
	var wg sync.WaitGroup

	id, err := http.Get(topURL)
	if err != nil {
		fmt.Errorf(err.Error())
	}

	defer id.Body.Close()
	json.NewDecoder(id.Body).Decode(&top)

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Title", "Url"})
	table.SetColWidth(300)
	wg.Add(30)
	for i := 0; i < 30; i++ {
		go makeNews(&news, top[i], table, &wg)
	}
	wg.Wait()
	table.Render()
}

// makeNews returns TOP 30 from hackernews
func makeNews(news *News, top int, table *tablewriter.Table, wg *sync.WaitGroup) {
	defer wg.Done()
	storyURL := fmt.Sprintf("https://hacker-news.firebaseio.com/v0/item/%s.json", strconv.Itoa(top))
	story, err := http.Get(storyURL)
	defer story.Body.Close()
	if err != nil {
		fmt.Errorf(err.Error())
	}
	json.NewDecoder(story.Body).Decode(&news)
	table.Append([]string{news.Title, news.URL})
}
