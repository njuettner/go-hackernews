[![Build Status](https://travis-ci.org/njuettner/go-hackernews.svg?branch=master)](https://travis-ci.org/njuettner/go-hackernews)
# Get the latest top hackernews

## Installation

* clone the repository

```
git clone ...
```

* get dependencies
```
go get github.com/PuerkitoBio/goquery
```

* install it
```
go install .
```

## How to use it

```
go-hackernews
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| India And Bangladesh Have Begun The Exchange Of Over 160 Enclaves             | http://www.washingtonpost.com/news/worldviews/wp/2015/08/01/say-goodbye-to-the-weirdest-border-dispute-in-the-world/?tid=HP_more?tid=HP_more       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Show HN: Glow – Syntax Highlighting For Clojure Source Code                   | http://blog.venanti.us/glow/                                                                                                                       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Saturated Reconstruction Of A Volume Of Neocortex                             | http://www.cell.com/cell/abstract/S0092-8674(15)00824-7                                                                                            |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Not The Retiring Type: People Still Working In Their 70s, 80s And 90s         | http://www.theguardian.com/lifeandstyle/2015/aug/01/still-working-aged-in-70s-80s-90s                                                              |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Frege’s Concept Horse Paradox In The Simply-Typed Λ-Calculus                  | http://dvt.name/2015/freges-concept-horse-paradox-in-the-simply-typed-%CE%BB-calculus/                                                             |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
...
```
